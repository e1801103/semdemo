package fi.vamk.e1801103.semdemo;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SemdemoApplication {
	@Autowired
	private AttendanceRepository repository;

	public static void main(String[] args) {
		SpringApplication.run(SemdemoApplication.class, args);
	}

	@Bean
	public void initData(){
		Attendance att = new Attendance("Formula1", new Date(2021, 8, 30));
		Attendance att2 = new Attendance("QWERTY", new Date(2021, 4, 30));
		Attendance att3 = new Attendance("ABCD", new Date(2021, 5, 25));
		repository.save(att);
		repository.save(att2);
		repository.save(att3);


	}

}
