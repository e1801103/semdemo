package fi.vamk.e1801103.semdemo;

//import static org.junit.Assert.assertThat;
import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Date;

import org.apache.commons.collections4.IterableUtils;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@Configuration
@ComponentScan(basePackages = {"fi.vamk.e1801103.semdemo"})
@EnableJpaRepositories(basePackageClasses = AttendanceRepository.class)

public class AttendanceControllerTests{
  @Autowired
  private AttendanceRepository repository;


    @Test
    public void postGetDeleteAttendance(){
      Iterable<Attendance> begin = repository.findAll();
      //System.out.println(IterableUtils.size(begin));
      //Given
        Attendance att = new Attendance("ABCDE", new Date(2021-04-25));
        System.out.println("ATT: " + att.ToString());
        //test save
        Attendance saved = repository.save(att);
        System.out.println("Saved: " + saved.ToString());
        //When
        Attendance found = repository.findByKey(att.getKey());
        System.out.println("Founded: " + found.ToString());
        Attendance found2 = repository.findByDate(att.getDate());
        System.out.println("Founded 2: " + found2.ToString());
        //Then
        assertThat(found.getKey()).isEqualTo(att.getKey());
        repository.delete(found);
        Iterable<Attendance> end = repository.findAll();
        //System.out.println(IterableUtils.size(end));
        assertEquals((long) IterableUtils.size(begin), (long) IterableUtils.size(end));


    }
}